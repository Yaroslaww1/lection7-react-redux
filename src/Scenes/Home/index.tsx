import React from 'react';
import Chat from '../../Containers/Chat';
import PageHeader from '../../Components/PageHeader';
import PageFooter from '../../Components/PageFooter';
import EditMessageModal from '../../Containers/EditMessage';

const HomePage: React.FC = () => {
  return (
    <>
      <PageHeader />
      <Chat />
      <PageFooter />
      <EditMessageModal />
    </>
  );
}

export default HomePage;
