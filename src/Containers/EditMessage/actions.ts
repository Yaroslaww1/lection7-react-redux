import { IMessage } from "../../interfaces";
import { EditMessageActionTypes, EditMessageActions } from "./types";

export function setIsEditing(isEditing: boolean, message? :IMessage): EditMessageActions {
  return {
    type: EditMessageActionTypes.SET_EDITING,
    isEditing,
    message
  }
}