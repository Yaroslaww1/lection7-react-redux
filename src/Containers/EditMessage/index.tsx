import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import PropTypes from 'prop-types';

import "./styles.css";
import { RootState } from '../../store';
import Modal from '../../Components/Modal';
import { setIsEditing } from './actions';
import { updateMessage, deleteMessage } from '../Chat/actions';

const mapState = (state: RootState) => ({
  isEditing: state.editMessage.isEditing,
  message: state.editMessage.message,
});

const mapDispatch = {
  setIsEditing,
  updateMessage,
  deleteMessage
}

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux;

const EditMessageModal: React.FC<Props> = ({
  isEditing,
  message,
  setIsEditing,
  updateMessage,
  deleteMessage
}) => {
  const handleCancel = () => setIsEditing(false);

  const handleSave = (newMessageText: string) => {
    if (message) {
      updateMessage({
        ...message,
        text: newMessageText,
        editedAt: (new Date()).toISOString()
      })
    }
    setIsEditing(false);
  }

  const handleDelete = () => {
    deleteMessage(message!.id);
    setIsEditing(false);
  }

  return (
    <>
      {isEditing &&
        <Modal
          header="Edit message"
          body={message?.text}
          onCancel={handleCancel}
          onSave={handleSave}
          additionalElements={[(
            <button
              key="modal-delete-button"
              className="modal-delete"
              onClick={handleDelete}
            >
              Delete
            </button>
          )]}
        />
      }
    </>
  );
}

EditMessageModal.propTypes = {
  isEditing: PropTypes.bool.isRequired
}

export default connector(EditMessageModal);
