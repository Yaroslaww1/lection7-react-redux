import { EditMessageActions, EditMessageActionTypes, EditMessageState } from './types';

const initialState: EditMessageState = {
  isEditing: false,
  message: {
    id: "",
    text: "AA",
    user: "",
    userId: "",
    avatar: "",
    editedAt: "",
    createdAt: "",
    isLiked: false
  }
}

export function editMessageReducer(
  state = initialState,
  action: EditMessageActions
): EditMessageState {
  switch (action.type) {
    case EditMessageActionTypes.SET_EDITING: 
      return {
        ...state,
        isEditing: action.isEditing,
        message: action.message
      }
    default:
      return state;
  }
}