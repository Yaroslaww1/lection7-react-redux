import { IMessage } from "../../interfaces";

export enum EditMessageActionTypes {
  SET_EDITING = 'SET_EDITING'
}

interface ISetEditingAction {
  type: EditMessageActionTypes.SET_EDITING
  isEditing: boolean,
  message?: IMessage
}

export type EditMessageActions = ISetEditingAction;

export interface EditMessageState {
  isEditing: boolean,
  message?: IMessage
}