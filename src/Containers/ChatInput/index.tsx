import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect, ConnectedProps } from 'react-redux';
import { createMessage } from '../../Services/messageService';
import { RootState } from '../../store';
import { addMessage } from '../Chat/actions';

import "./styles.css";

const mapState = (state: RootState) => ({
  currentUser: state.system.currentUser
});

const mapDispatch = {
  addMessage
}

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux;

const ChatInput: React.FC<Props> = ({
  addMessage: addMessageProps,
  currentUser
}) => {
  const [messageBody, setMessageBody] = useState<string>('');

  const handleChange = (event: React.FormEvent<HTMLTextAreaElement>) => {
    const messageBody = event.currentTarget.value;
    setMessageBody(messageBody);
  }

  const addMessage = () => {
    const message = createMessage({ messageBody, user: currentUser });
    addMessageProps(message);
    setMessageBody('');
  }

  return (
    <div className="chat-input-wrapper">
      <textarea
        placeholder="Enter your message here"
        value={messageBody}
        onChange={handleChange}
      />
      <button
        onClick={addMessage}
      >
        Send
        <i className="fa fa-paper-plane" aria-hidden="true"></i>
      </button>
    </div>
  );
}

ChatInput.propTypes = {
  addMessage: PropTypes.func.isRequired,
  currentUser: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired
  }).isRequired
}

export default connector(ChatInput);
