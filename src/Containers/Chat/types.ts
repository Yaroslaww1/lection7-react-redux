import { IMessage } from "../../interfaces";

export enum ChatActionTypes {
  SET_MESSAGES = 'SET_MESSAGES',
  ADD_MESSAGE = 'ADD_MESSAGE',
  TOGGLE_LIKE_MESSAGE = 'LIKE_MESSAGE',
  DELETE_MESSAGE = 'DELETE_MESSAGE',
  UPDATE_MESSAGE = 'UPDATE_MESSAGE'
}

interface ISetMessagesAction {
  type: ChatActionTypes.SET_MESSAGES
  messages: IMessage[]
}

interface IAddMessageAction {
  type: ChatActionTypes.ADD_MESSAGE
  message: IMessage
}

interface IToggleLikeMessageAction {
  type: ChatActionTypes.TOGGLE_LIKE_MESSAGE
  messageId: string
}

interface IDeleteMessageAction {
  type: ChatActionTypes.DELETE_MESSAGE
  messageId: string
}

interface IUpdateMessageAction {
  type: ChatActionTypes.UPDATE_MESSAGE
  message: IMessage
}

export type ChatActions = 
  ISetMessagesAction
| IAddMessageAction
| IToggleLikeMessageAction
| IDeleteMessageAction
| IUpdateMessageAction;

export interface ChatState {
  messages: IMessage[]
}