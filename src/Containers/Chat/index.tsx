import React, { useCallback, useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import Spinner from '../../Components/Spinner';
import ChatHeader from '../../Components/ChatHeader';
import ChatMain from '../../Components/ChatMain';
import { getMessages } from '../../Services/messageService';
import ChatInput from '../ChatInput';

import "./styles.css";
import { 
  setMessages,
  toggleLikeMessage,
} from './actions';
import { setIsLoading } from '../../System/actions';
import { setIsEditing } from '../EditMessage/actions';
import { RootState } from '../../store';
import { IMessage } from '../../interfaces';

const mapState = (state: RootState) => ({
  messages: state.chat.messages,
  currentUser: state.system.currentUser,
  isLoading: state.system.isLoading
});

const mapDispatch = {
  setMessages,
  setIsLoading,
  toggleLikeMessage,
  setIsEditing
}

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux;

const Chat: React.FC<Props> = ({
  messages,
  currentUser,
  isLoading,
  setMessages,
  setIsLoading,
  toggleLikeMessage,
  setIsEditing
}) => {
  const keyboardListener = useCallback((event: KeyboardEvent) => {
    const keyCode = event.code;
    if (keyCode === 'ArrowUp') {
      const lastMessageIndex = messages.reduce((acc, message, index) => (
        message.userId === currentUser.id ? index : acc
      ), -1);
      if (lastMessageIndex > 0) {
        const lastMessage = messages[lastMessageIndex];
        setIsEditing(true, lastMessage);
      }
    }
  }, [messages, currentUser, setIsEditing])

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);

      const messages = await getMessages();
      setMessages(messages);

      setIsLoading(false);
    }
    fetchData();
  }, []);

  useEffect(() => {
    document.addEventListener('keydown', keyboardListener);
    return () => document.removeEventListener('keydown', keyboardListener);
  }, [keyboardListener])

  const toggleLike = (messageId: string) => toggleLikeMessage(messageId);

  const openEditingModal = (message: IMessage) => {
    setIsEditing(true, message);
  }

  return (
    <>
      {isLoading ?
        <Spinner />
        :
        <div className="chat-wrapper">
          <ChatHeader
            messages={messages}
            chatName="Chat name"
          />
          <ChatMain
            messages={messages}
            currentUser={currentUser!}
            messageFunction={{
              onLike: toggleLike,
              onEdit: openEditingModal
            }}
          />
          <ChatInput />
        </div>
      }
    </>
  );
}

export default connector(Chat);
