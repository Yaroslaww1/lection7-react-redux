import { IMessage } from "../../interfaces";
import { ChatActions, ChatActionTypes } from "./types";

export function setMessages(messages: IMessage[]): ChatActions {
  return {
    type: ChatActionTypes.SET_MESSAGES,
    messages
  }
}

export function addMessage(message: IMessage): ChatActions {
  return {
    type: ChatActionTypes.ADD_MESSAGE,
    message
  }
}

export function toggleLikeMessage(messageId: string): ChatActions {
  return {
    type: ChatActionTypes.TOGGLE_LIKE_MESSAGE,
    messageId
  }
}

export function deleteMessage(messageId: string): ChatActions {
  return {
    type: ChatActionTypes.DELETE_MESSAGE,
    messageId
  }
}

export function updateMessage(message: IMessage): ChatActions {
  return {
    type: ChatActionTypes.UPDATE_MESSAGE,
    message
  }
}
