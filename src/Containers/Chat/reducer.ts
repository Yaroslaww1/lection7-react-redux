import { ChatActions, ChatActionTypes, ChatState } from './types';

const initialState: ChatState = {
  messages: []
}

export function chatReducer(
  state = initialState,
  action: ChatActions
): ChatState {
  switch (action.type) {
    case ChatActionTypes.SET_MESSAGES: 
      return {
        ...state,
        messages: action.messages
      }
    case ChatActionTypes.ADD_MESSAGE:
      return {
        ...state,
        messages: [...state.messages, action.message]
      }
    case ChatActionTypes.TOGGLE_LIKE_MESSAGE: 
      return {
        ...state,
        messages: state.messages.map(message => {
          if (message.id === action.messageId)
            message.isLiked = !message.isLiked;
          return message;
        })
      }
    case ChatActionTypes.DELETE_MESSAGE:
      return {
        ...state,
        messages: state.messages.filter(message => message.id !== action.messageId)
      }
    case ChatActionTypes.UPDATE_MESSAGE:
      return {
        ...state,
        messages: state.messages.map(message => {
          if (message.id === action.message.id)
            return action.message;
          return message;
        })
      }
    default:
      return state;
  }
}