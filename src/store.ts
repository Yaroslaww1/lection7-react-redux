import {
  createStore,
  combineReducers
} from 'redux';

import { chatReducer } from './Containers/Chat/reducer';
import { systemReducer } from './System/reducer';
import { editMessageReducer } from './Containers/EditMessage/reducer';

const initialState = {};

const reducers = {
  chat: chatReducer,
  system: systemReducer,
  editMessage: editMessageReducer
};

const rootReducer = combineReducers({
  ...reducers
});

const store = createStore(
  rootReducer,
  initialState
);

export type RootState = ReturnType<typeof rootReducer>;

export default store;
