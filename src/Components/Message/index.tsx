import React from 'react';
import PropTypes from 'prop-types';
import { IMessage } from '../../interfaces';
import LikeIcon from '../MessageIcons/likeIcon';
import MessageTime from './messageTime';

import "./styles.css";

type MessageProps = {
  message: IMessage,
  onLike: () => void
}

const Message: React.FC<MessageProps> = ({
  message,
  onLike
}) => {
  return (
    <div className="chat-message">
      <img src={message.avatar} alt="User avatar"></img>
      <div className="message-body">
        {message.text}
        <MessageTime
          message={message}
        />
      </div>
      <LikeIcon
        isLiked={message.isLiked}
        onLike={onLike}
      />
    </div>
  );
}

Message.propTypes = {
  message: PropTypes.shape({
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    editedAt: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    isLiked: PropTypes.bool.isRequired
  }).isRequired,
  onLike: PropTypes.func.isRequired
}

export default Message;
