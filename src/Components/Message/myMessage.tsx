import React from 'react';
import PropTypes from 'prop-types';
import { IMessage } from '../../interfaces';
import EditIcon from '../MessageIcons/editIcon';
import MessageTime from './messageTime';

import "./styles.css";

type MyMessageProps = {
  message: IMessage,
  onEdit: () => void
}

const MyMessage: React.FC<MyMessageProps> = ({
  message,
  onEdit
}) => {
  return (
    <div className="chat-message my-message">
      <div className="message-body">
        {message.text}
        <MessageTime
          message={message}
        />
      </div>
      <div className="message-edit">
        <EditIcon
          onEdit={onEdit}
        />
      </div>
    </div>
  );
}

MyMessage.propTypes = {
  message: PropTypes.shape({
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    editedAt: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    isLiked: PropTypes.bool.isRequired
  }).isRequired,
  onEdit: PropTypes.func.isRequired
}

export default MyMessage;
