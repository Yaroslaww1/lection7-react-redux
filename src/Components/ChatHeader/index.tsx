import React from 'react';
import PropTypes from 'prop-types';
import { getDate } from '../../helpers/timeHelpers';

import "./styles.css";
import { IMessage } from '../../interfaces';

type ChatHeaderProps = {
  messages: IMessage[],
  chatName: string
}

const getLastMessageTime = (messages: IMessage[]) => {
  const lastMessage = messages.reduce(function(m1, m2) {
    const date1 = new Date(m1.createdAt);
    const date2 = new Date(m2.createdAt);
    return date1 > date2 ? m1 : m2;
  }, { createdAt: "0" });
  return new Date(lastMessage.createdAt);
}

const getParticipantsCount = (messages: IMessage[]) => {
  const participantsMap: Record<string, boolean> = {};
  for (const message of messages) {
    participantsMap[message.userId] = true;
  }
  return Object.keys(participantsMap).length;
}


const getParticipantsString = (participantsCount: number) => 
  `${participantsCount} participants`

const getMessagesString = (messagesCount: number) => 
  `${messagesCount} messages`

const getLastMessageTimeString = (lastMessageTime: Date) => 
  `last message at ${getDate(lastMessageTime.toISOString())}`

const ChatHeader: React.FC<ChatHeaderProps> = ({
  messages,
  chatName
}) => {
  return (
    <div className="chat-header">
      <div className="text">
        {chatName}
      </div>
      <div className="text">
        {getParticipantsString(getParticipantsCount(messages))}
      </div>
      <div className="text">
        {getMessagesString(messages.length)}
      </div>
      <div className="text">
        {getLastMessageTimeString(getLastMessageTime(messages))}
      </div>
    </div>
  );
}

ChatHeader.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    editedAt: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    isLiked: PropTypes.bool.isRequired
  }).isRequired).isRequired,
  chatName: PropTypes.string.isRequired
}

export default ChatHeader;