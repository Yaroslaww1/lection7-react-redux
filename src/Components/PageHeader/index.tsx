import React from 'react';

import "./styles.css";

const PageHeader: React.FC = () => {
  return (
    <div className="page-header">
      <img src="./img/bsa-logo.png" alt="logo"></img>
    </div>
  );
}

export default PageHeader;
