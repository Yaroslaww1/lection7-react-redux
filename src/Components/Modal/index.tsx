import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import "./styles.css";

type ModalProps = {
  header?: string,
  body?: string,
  onSave: (newBody: string) => void,
  onCancel: () => void
  additionalElements?: JSX.Element[]
}

const Modal: React.FC<ModalProps> = ({
  header,
  body: bodyProps,
  onSave,
  onCancel,
  additionalElements
}) => {
  const [body, setBody] = useState(bodyProps || '');

  useEffect(() => {
    setBody(bodyProps || '');
  }, [bodyProps])

  const handleChange = (event: React.FormEvent<HTMLTextAreaElement>) => {
    const newBody = event.currentTarget.value;
    setBody(newBody);
  }

  return (
    <div className="modal-background">
      <div className="modal">
        <div className="modal-header">
          {header}
        </div>
        <div className="line">
        </div>
        <textarea
          className="modal-body"
          placeholder="Enter your text here"
          value={body} 
          onChange={handleChange}
        />
        <div className="modal-footer">
          <div className="main">
            <button
              className="modal-save"
              onClick={() => onSave(body)}
            >
              Save
            </button>
            <button
              className="modal-close"
              onClick={onCancel}
            >
              Cancel
            </button>
          </div>
          <div className="additional">
            {additionalElements}
          </div>
        </div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  header: PropTypes.string,
  body: PropTypes.string,
  onCancel: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  additionalElements: PropTypes.array
}

Modal.defaultProps = {
  header: 'Modal header',
  body: 'Modal body'
}

export default Modal;
