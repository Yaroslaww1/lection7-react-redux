import { v4 as uuidv4 } from 'uuid';
import { IMessage, IUser } from "../interfaces";

const API_URL = 'https://edikdolynskyi.github.io/react_sources/messages.json';

export async function getMessages(): Promise<IMessage[]> {
  const request = await fetch(API_URL, {
    method: 'GET'
  });
  const messages = (await request.json() as IMessage[])
    .map(message => 
      ({ ...message, isLiked: false })
    );
  return messages;
}

export interface createMessageProps { 
  messageBody: string,
  user: IUser
}

export function createMessage(obj: createMessageProps): IMessage {
  const { messageBody, user } = obj;
  const message: IMessage = {
    id: uuidv4(),
    text: messageBody,
    user: user.name || "",
    userId: user.id,
    avatar: user.avatar,
    editedAt: "",
    createdAt: (new Date()).toISOString(),
    isLiked: false
  }
  return message;
}