import { SystemActions, SystemActionTypes } from "./types";

export function setIsLoading(isLoading: boolean): SystemActions {
  return {
    type: SystemActionTypes.SET_LOADING,
    isLoading
  }
}