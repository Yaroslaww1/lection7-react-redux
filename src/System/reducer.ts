import { SystemActions, SystemActionTypes, SystemState } from './types';
import { v4 as uuidv4 } from 'uuid';

const initialState: SystemState = {
  currentUser: {
    id: uuidv4(),
    name: "Yaroslav",
    avatar: ""
  },
  isLoading: false
}

export function systemReducer(
  state = initialState,
  action: SystemActions
): SystemState {
  switch (action.type) {
    case SystemActionTypes.SET_LOADING: 
      return {
        ...state,
        isLoading: action.isLoading
      }
    default:
      return state;
  }
}