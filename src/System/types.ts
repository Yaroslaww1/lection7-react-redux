import { IUser } from "../interfaces";

export enum SystemActionTypes {
  SET_LOADING = 'SET_LOADING'
}

interface ISetLoadingAction {
  type: SystemActionTypes.SET_LOADING
  isLoading: boolean
}

export type SystemActions = ISetLoadingAction;

export interface SystemState {
  currentUser: IUser,
  isLoading: boolean
}